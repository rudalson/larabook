<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::resource('/articles', 'ArticlesController');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('mail', function() {
	$article = App\Article::with('user')->find(1);

	return Mail::send(
		'emails.articles.created',
		compact('article'),
		function($message) use ($article) {
			$message->to('rudalson@gmail.com');
			$message->subject('새 글이 등록되었습니다 -'.$article->title);
		}

	);
});

Route::get('markdown', function() {
	$text =<<<EOT
# 마크다운 예제 1

이 문서는 [마크다운][1]으로 썼습니다. 화면에는 HTML로 변환되어 출력됩니다.

## 순서 없는 목록

- 첫 번째 항목
- 두 번재 항목[^1]

[1]: http://daringfireball.net/projects/markdown


[^1]: 두 번째 항목_ http://google.com
EOT;

	return app(ParsedownExtra::class)->text($text);
});

Route::get('docs/{file?}', 'DocsController@show');

Route::get('docs/images/{image}', 'DocsController@image')
	->where('image', '[\pL-\pN\._-]+-img-[0-9]{2}.png');

// Event::listen('article.created', function($article) {
// 	var_dump('이벤트를 받았습니다. 받은 데이터(상태)는 다음과 같습니다.');
// 	var_dump($article->toArray());
// });

// DB::listen(function ($query) {
// 	var_dump($query->sql);
// });