<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel Study</title>
</head>
@yield('style')

<body>

@yield('content')

</body>

@yield('script')
</html>