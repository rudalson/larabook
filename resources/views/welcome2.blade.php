@extends('layouts.master')

@section('style')
	<style>
		body {background: green; color:white;}
	</style>
@endsection

@section('content')
	<p>I'm a Child view's 'content' section</p>
	@include('partials.footer')
@endsection

@section('script')
	
	<script>
		alert("I'm a Child view's 'script' section");
	</script>
@endsection